[general]
path=/home/merly/git/ngsphy/test
project_name=test_indelible
ploidy=1
[data]
origin=2
indelible_control=/home/merly/git/ngsphy/test/indelible1/control.3.txt
newick_file=/home/merly/git/ngsphy/test/indelible1/t2.tree
[coverage]
experiment=F:10
locus=LN:1.5,1
[ngs-reads-art]
amp=true
c=100
ef=false
l=150
m=250
p=true
q=true
s=50
sam=true
[execution]
environment = bash
run = off
threads=4
